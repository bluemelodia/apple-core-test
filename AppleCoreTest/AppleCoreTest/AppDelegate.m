//
//  AppDelegate.m
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "AppDelegate.h"
#import "AppleTreeInfo.h"
#import "AppleTreeDetails.h"
#import "AppleTableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    /* Grab a pointer to the managed object context using helper function that comes with the template */
    NSManagedObjectContext *context = [self managedObjectContext];
    
    /*  Create new instance of NSManagedObject for appleTreeInfo entity, calling insertNewObjectForEntityForName: every object stored in Core Data derives from NSManagedObject ->
        once you have an instance of the object, you can call setValue for any attribute in the .xcdatamodel 
        file */
    //NSManagedObject *appleTreeInfo = [NSEntityDescription insertNewObjectForEntityForName:@"AppleTreeInfo" inManagedObjectContext:context];
    /*[appleTreeInfo setValue:[NSNumber numberWithInt:24] forKey:@"age"];
    [appleTreeInfo setValue:@"New York Penn Station" forKey:@"location"];
    [appleTreeInfo setValue:@"Arthur" forKey:@"name"];*/
    AppleTreeInfo *appleTreeInfo = [NSEntityDescription insertNewObjectForEntityForName:@"AppleTreeInfo" inManagedObjectContext:context];
    appleTreeInfo.age = [NSNumber numberWithInt:24];
    appleTreeInfo.location = @"New York Penn Station";
    appleTreeInfo.name = @"Arthur";
    
    /*NSManagedObject *appleTreeDetails = [NSEntityDescription insertNewObjectForEntityForName:@"AppleTreeDetails" inManagedObjectContext:context];
    [appleTreeDetails setValue:[NSDate date] forKey:@"bakeDate"];
    [appleTreeDetails setValue:[NSDate date] forKey:@"cutDate"];
    [appleTreeDetails setValue:[NSNumber numberWithInt:15] forKey:@"numberOfPies"];
    
    [appleTreeDetails setValue:appleTreeInfo forKey:@"info"];
    [appleTreeInfo setValue:appleTreeDetails forKey:@"details"];*/
    AppleTreeDetails *appleTreeDetails = [NSEntityDescription insertNewObjectForEntityForName:@"AppleTreeDetails" inManagedObjectContext:context];
    appleTreeDetails.bakeDate = [NSDate date];
    appleTreeDetails.cutDate = [NSDate date];
    appleTreeDetails.numberOfPies = [NSNumber numberWithInt:15];
    appleTreeDetails.info = appleTreeInfo;
    appleTreeInfo.details = appleTreeDetails;
    
    /* at this point, the objects are modified in memory but not stored in the database */
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"We could not save the apple tree: %@", [error localizedDescription]);
    }
    
    /* list objects currently in database */
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AppleTreeInfo" inManagedObjectContext:context]; /* get a poitner to the AppleTreeInfo entity we want to retrieve */
    [fetchRequest setEntity:entity]; /* tell the fetch request that this is the kind of Entity we want */
    
    /* pull all of the objects in the AppleTreeInfo table onto the Managed Object Context*/
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    /* iterate through each NSManagedObject, and extract the data */
    for (AppleTreeInfo *info in fetchedObjects) {
        NSLog(@"Name: %@", info.name);
        AppleTreeDetails *details = info.details;
        /* can also access AppleTreeDetails object through this details property */
        NSLog(@"Number of pies: %@", details.numberOfPies);
    }
    
    AppleTableViewController *controller = [[AppleTableViewController alloc] init]; /* get the view controller at the top of the navigation stack */
    controller.managedObjectContext = self.managedObjectContext;
    self.window.rootViewController = controller;

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

/* call this to retrieve, insert, or delete objects in the database */
@synthesize managedObjectContext = _managedObjectContext;
/* contains definitions for each object stored in the database - can setup objects in visual editor (.xcdatamodel) or code */
@synthesize managedObjectModel = _managedObjectModel;
/* database connection - set up actual names and locations of what databases will be used to store objects - any time a managed object context needs to save something, it goes through this coordinator */
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "melanie.hsu.AppleCoreTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AppleCoreTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AppleCoreTest.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
