//
//  AppleTreeDetails.h
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AppleTreeInfo;

@interface AppleTreeDetails : NSManagedObject

@property (nonatomic, retain) NSDate * cutDate;
@property (nonatomic, retain) NSDate * bakeDate;
@property (nonatomic, retain) NSNumber * numberOfPies;
@property (nonatomic, retain) AppleTreeInfo *info;

@end
