//
//  AppleTreeInfo.h
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AppleTreeDetails;

@interface AppleTreeInfo : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) AppleTreeDetails *details;

@end
