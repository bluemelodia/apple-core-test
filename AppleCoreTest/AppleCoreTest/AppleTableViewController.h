//
//  AppleTableViewController.h
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppleTableViewController : UITableViewController

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSArray *appleTreeInfos; /* this already has the managed object context that will be used to retrieve this list - set up in AppDelegate.m */

@end
