//
//  AppleTreeInfo.m
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "AppleTreeInfo.h"
#import "AppleTreeDetails.h"


@implementation AppleTreeInfo

@dynamic name;
@dynamic age;
@dynamic location;
@dynamic details;

@end
