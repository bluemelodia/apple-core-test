//
//  AppleTreeDetails.m
//  AppleCoreTest
//
//  Created by Melanie Lislie Hsu on 8/14/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import "AppleTreeDetails.h"
#import "AppleTreeInfo.h"


@implementation AppleTreeDetails

@dynamic cutDate;
@dynamic bakeDate;
@dynamic numberOfPies;
@dynamic info;

@end
